define([
    "jquery"
], function($) {
    return {
        defaultStatus: 'thinkin...',
        disconnected: {},
        lock: {},
        add: function (data) {
            if (data.hasOwnProperty('playerId')) {
                var seat = $('.opponents .seat:not(.taken)').first();
                seat.attr('id', data.playerId).addClass('taken');
                if (data.status) {
                    seat.find('.status .text').text(data.status);
                }
            }
        },
        update: function (players) {
            var self = this;
            players.forEach(function (data) {
                if (data.disconnected) {
                    self.disconnected[data.playerId] = true;
                    $('.opponents #' + data.playerId + ' .status .text').text('Disconnected.');
                }
                if (self.disconnected[data.playerId]) {return;}
                if (!$('.opponents #' + data.playerId).length) {
                    self.add(data);
                }
                if (data.name) {
                    $('.opponents #' + data.playerId + ' .name').text(data.name);
                }
                if (data.status) {
                    $('.opponents #' + data.playerId + ' .status .text').text(data.status);
                }
                if (data.hasOwnProperty('onGoing')) {
                    $('.opponents #' + data.playerId + ' .icon').addClass(data.onGoing ? 'green' : 'red');
                    self.lock[data.playerId] = !data.onGoing;
                }
            });
        },
        reset: function () {
            var self = this;
            $('.opponents .seat.taken').each(function () {
                var playerId = $(this).attr('id');
                if (self.disconnected[playerId]) {return;}
                $(this).find('.status .text').text(self.lock[playerId] ? 'Left' : self.defaultStatus);
                if (!self.lock[playerId]) {
                    $(this).find('.status .icon').removeClass('green').removeClass('red');
                }
            });
        }
    }
});

