define([
    "jquery"
], function($) {
    return {
        update: function (data) {
            if (data.hasOwnProperty('count')) {
                $('.discard .count').text(data.count);
            }
            if (data.hasOwnProperty('img_url')) {
                $('.discard > img').attr('src', data.img_url);
            }
        }
    }
});

