define([
    "jquery",
    "app/components/opponents",
    "app/components/profile"
], function($, opponents, profile) {
    return {
        turn: '',
        update: function (data) {
            if (data.round) {
                $('.round .label').text(data.round);
                opponents.lock = {}; 
                opponents.reset();
                profile.lock = false;
                profile.reset();
            }
            if (data.turn && this.turn !== data.turn) {
                this.turn = data.turn;
            }
        }
    }
});

