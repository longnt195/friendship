define([
    "jquery",
    "app/components/profile"
], function($, profile) {
    return {
        update: function (data) {
            data.forEach(function (player) {
                var className = player.self ? 'self' : '',
                    area = player.hasOwnProperty('id') ? 'tbody' : 'tfoot',
                    template = '<tr class="' + className + '">' +
                        '<th scope="row">' + player.name + '</th>' +
                            '<td><span class="coin">' + player.chest.coins + '</span></td>' +
                            '<td><span class="gem">' + player.chest.gems + '</span></td>' +
                            '<td><span class="total">' + player.chest.total + '</span></td>' +
                    '</tr>';
                $(template).appendTo('#score > table > ' + area);
            });
            $('#score').show();
        }
    }
});

