define([
    "jquery"
], function($) {
    return {
        update: function (data) {
            if (data.hasOwnProperty('count')) {
                $('.deck .count').text(data.count);
            }
        }
    }
});

