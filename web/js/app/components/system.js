define([
    "jquery"
], function($) {
    return {
        update: function (data) {
            if (data.hasOwnProperty('message')) {
                $('.system > p').text(data.message);
            }
        }
    }
});

