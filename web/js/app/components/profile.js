define([
    "jquery"
], function($) {
    return {
        id: '',
        name: '',
        lock: false,
        update: function (data) {
            if (data.hasOwnProperty('tmpCoin')) {
                $('.profile .right .tmp .coin').text(data.tmpCoin);
            }
            if (data.chest) {
                $('.profile .right .chest .coin').text(data.chest['coins']);
                $('.profile .right .chest .gem').text(data.chest['gems']);
                if (data.lock) {
                    this.lock = data.lock;
                    this.reset();
                }
            }
            if (data.status) {
                $('.profile .center .status .text').text(data.status);
            }
        },
        reset: function () {
            this.update({'status': this.lock ? 'Left' : "Make a right decision"});
            var decisionEle = $('.profile .decision');
            decisionEle.attr('data-select', '');
            if (this.lock) {
                decisionEle.addClass('hidden');
            } else {
                decisionEle.removeClass('hidden');
            }
        }
    }
});

