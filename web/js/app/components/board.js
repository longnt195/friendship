define([
    "jquery",
    "app/components/profile",
    "app/components/opponents"
], function($, profile, opponents) {
    return {
        add: function (card) {
            var template = '<div id="' + card.entity_id + '" class="card ' + card.type + '">' +
                '<img src="' + card.img_url + '" alt="' + card.card_id + '" /><div>';
            if (card.type === 'coin' || card.type === 'gem') {
                template += '<span class="value ' + card.type +'">' + card.value + '</span>';
            }
            if (card.type === 'coin') {
                template += ' / <span class="orig-value">' + card.orig_value + '</span>';
            }
            template += '</div></div>';
            $(template).appendTo('.board .cards');
            $('.message > p').text(card.name);
            opponents.reset();
            profile.reset();
        },
        update: function (data) {
            if (data.hasOwnProperty('message')) {
                $('.message').attr('data-type', data.type || '');
                $('.message > p').text(data.message);
            }
            if (data.cards) {
                data.cards.forEach(function (card) {
                    $('.cards #' + card.entity_id + ' .value').text(card.value);
                });
            }
            if (data.clear) {
                this.clear();
            }
        },
        clear: function () {
            $('.board .card').remove();
        }
    }
});

