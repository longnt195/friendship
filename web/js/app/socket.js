define([
    "jquery",
    "app/components/profile"
], function($, profile) {
    return {
        conn: false,
        initialize: function () {
            var self = this,
                origin = window.location.hostname || 'localhost',
                url = new URL(window.location.origin),
                protocol = url.protocol === 'https:' ? 'wss://' : 'ws://',
                params = new window.URLSearchParams(window.location.search),
                port = params.get('port'),
                socket = protocol + origin + (port ? ':' + port : '');
            this.conn = new WebSocket(socket);
            this.conn.onopen = function (e) {
                self.send(JSON.stringify({action: 'join', name: profile.name}));
                self.send(JSON.stringify({action: 'getOpponents'}));
                self.displayScreen();
            }
            this.conn.onerror = function (e) {
                self.displayScreen('login');
                window.location.reload();
            }
            this.conn.onclose = function (e) {
                self.displayScreen('login');
                window.location.reload();
            }
            return this.conn;
        },
        send: function (msg) {
            this.conn.send(msg);
        },
        displayScreen: function(screen) {
            if (screen === 'login') {
                $('#login').show();
                $('#wrapper').hide();
            } else {
                $('#login').hide();
                $('#wrapper').show();
            }
        }
    }
});

