define([
    "jquery",
    "app/socket",
    "app/components/system",
    "app/components/board",
    "app/components/deck",
    "app/components/round",
    "app/components/discard",
    "app/components/profile",
    "app/components/opponents",
    "app/components/score",
], function($, socket, system, board, deck, round, discard, profile, opponents, score) {
    var lock = false;
    var aboard = function () {
        var conn = socket.initialize();
        $('#go').off().on('click', function (e) {
            $(this).parent('.decision').attr('data-select', 'go');
            profile.update({'status':"Mooing forward..."});
            socket.send(JSON.stringify({action: 'go'}));
        });
        $('#leave').off().on('click', function (e) {
            $(this).parent('.decision').attr('data-select', 'leave');
            profile.update({'status':"You've chosen to leave."});
            socket.send(JSON.stringify({action: 'leave'}));
        });
        $('#exit').on('click', function (e) {
            window.location.reload();
        });

        conn.onmessage = function (e) {
            if (lock) {return false;}
            var res = JSON.parse(e.data);
            switch (res.area) {
                case 'board':
                    board[res.action](res.data);
                    break;
                case 'deck':
                    deck[res.action](res.data);
                    break;
                case 'profile':
                    profile[res.action](res.data);
                    break;
                case 'round':
                    round[res.action](res.data);
                    break;
                case 'discard':
                    discard[res.action](res.data);
                    break;
                case 'opponents':
                    opponents[res.action](res.data);
                    break;
                case 'system':
                    system[res.action](res.data);
                    break;
                case 'score':
                    score[res.action](res.data);
                    lock = true;
                    break;
            }
            console.log(e.data);
        };
    };

    //login screen
    $('#login form').on('submit', function(e) {
        e.preventDefault();
        profile.name = $('#username').val();
        $('.profile .center .name > h2').text(profile.name);
        aboard();
    });
    $('input#username').focus();
});