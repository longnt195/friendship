requirejs.config({
    baseUrl: 'web/js',
    paths: {
        app: '../js/app',
        jquery: 'lib/jquery-3.6.0.min'
    }
});

requirejs(['app/main']);