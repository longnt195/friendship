<?php
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use FriendShip\App\Main;
use FriendShip\App\Cards\Deck;
use FriendShip\App\Play\Board;
use FriendShip\App\Play\Round;
use FriendShip\App\Client;

require './vendor/autoload.php';
$client = new Client();
$deck = new Deck($client);
$round = new Round($client);
$board = new Board($deck, $round, $client);
$port = $argv[1] ?? 80;
$address = $argv[2] ?? '0.0.0.0';
$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Main($deck, $board, $round, $client)
        )
    ),
    $port,
    $address
);

$client->setServer($server);
$server->run();