<?php
namespace FriendShip\App\Player;

use FriendShip\App\Cards\Card;

class Chest
{
    protected int $coins = 0;
    /**
     * @var Card[]
     */
    protected array $gems = [];
    
    public function addCoin($coins): void
    {
        $this->coins += $coins;
    }

    public function addGems(array $gems): void
    {
        $this->gems = array_merge($this->gems, $gems);
    }
    
    public function getTotal(): array
    {
        $gemValue = 0;
        foreach ($this->gems as $gem) {
            $gemValue += $gem->getValue();
        }
        return [
            'coins' => $this->coins,
            'gems' => $gemValue,
            'total' => $this->coins + $gemValue
        ];
    }
}