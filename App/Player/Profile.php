<?php
namespace FriendShip\App\Player;

use FriendShip\App\Resource\DataObject;
use FriendShip\App\Api\ActionInterface;
use FriendShip\App\Client;

class Profile extends DataObject
{
    private Chest $chest;
    
    protected $connectionId = '';
    protected string $name = '';
    protected int $tmpCoin = 0;

    protected array $decision;
    protected bool $isOnGoing = true;
    private Client $client;
    private string $status = 'Thinkin...';

    /**
     * @param Client $client
     * @param array $data
     */
    public function __construct(
        Client $client,
        array $data = []
    ) {
        parent::__construct($data);
        $this->chest = new Chest();
        $this->client = $client;
    }

    public function setName(string $name): self
    {
         $this->name = $name;
         return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }
    
    public function isOnGoing(): bool
    {
        return $this->isOnGoing;
    }
    
    public function setIsOnGoing(bool $going): self
    {
        $this->isOnGoing = $going;
        return $this;
    }
    
    public function decide(string $action, string $turnId): bool
    {
        if (in_array($action, [ActionInterface::GO, ActionInterface::LEAVE])) {
            $this->decision[$turnId] = $action;
            $this->setStatus('Ready!');
            $this->client->sendOthers($this->getConnectionId(), ['area' => 'opponents', 'action' => 'update', 'data' => [[
                'status' => $this->getStatus(),
                'playerId' => $this->getConnectionId()
            ]]]);
            return true;
        }
        return false;
    }

    public function getDecision(string $turnId): string
    {
        return $this->decision[$turnId] ?? '';
    }
    
    public function addTmpCoin(int $coin): void
    {
        $this->tmpCoin += $coin;
    }
    
    public function storeCoins(): void
    {
        $this->chest->addCoin($this->tmpCoin);
        $this->tmpCoin = 0;
    }
    
    public function storeGems(array $gems): void
    {
        $this->chest->addGems($gems);
    }
    
    public function getChest(): Chest
    {
        return $this->chest;
    }
    
    public function reset(): void
    {
        $this->tmpCoin = 0;
        $this->isOnGoing = true;
        $this->client->send($this->getConnectionId(), ['area' => 'profile', 'action' => 'update', 'data' => [
            'tmpCoin' => $this->getTmpCoin()
        ]], 3);
    }
    
    public function getTmpCoin(): int
    {
        return $this->tmpCoin;
    }
    
    public function setConnectionId($id): self
    {
        $this->connectionId = $id;
        return $this;
    }

    public function getConnectionId()
    {
        return $this->connectionId;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }
}