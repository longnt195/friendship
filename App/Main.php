<?php
namespace FriendShip\App;

use FriendShip\App\Api\ActionInterface;
use FriendShip\App\Cards\Deck;
use FriendShip\App\Exception\Trap;
use FriendShip\App\Play\Board;
use FriendShip\App\Play\Round;
use FriendShip\App\Player\Profile;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

class Main implements MessageComponentInterface {
    private Deck $deck;
    private Board $board;
    private Round $round;
    private Client $clients;

    /**
     * @param Deck $deck
     * @param Board $board
     * @param Round $round
     * @param Client $clients
     */
    public function __construct(
        Deck $deck,
        Board $board,
        Round $round,
        Client $clients
    ) {
        $this->clients = $clients;
        $this->deck = $deck;
        $this->board = $board;
        $this->round = $round;
    }
    
    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        if (count($this->clients->getPlayers()) >= Board::MAX_PLAYERS || $this->clients->getPlayerById($conn->resourceId)
        || $this->round->getTurnId() !== '1.1') {
            $this->disconnect($conn);
        } else {
            $this->clients->getClients()->attach($conn);
            $player = $this->clients->addPlayer($conn);
            $this->clients->sendOthers($conn->resourceId, ['area' => 'opponents', 'action' => 'add', 'data' => [[
                'playerId' => $conn->resourceId,
                'status' => $player->getStatus()
            ]]]);
            echo "New connection! ({$conn->resourceId})\n";
        }
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        /** @var Profile $player */
        $player = $this->clients->getPlayerById($from->resourceId);
        $msg = json_decode($msg, true);
        $turnId = $this->round->getTurnId();

        $continue = false;
        if (in_array($msg['action'] , [ActionInterface::GO, ActionInterface::LEAVE])) {
            $player->decide($msg['action'], $turnId);
            $continue = true;
        } else if ($msg['action'] == ActionInterface::JOIN) {
            $player->setName($msg['name'] ?? 'Captain ' . $player->getConnectionId());
            $this->clients->sendOthers($player->getConnectionId(), ['area' => 'opponents', 'action' => 'update', 'data' => [[
                'playerId' => $player->getConnectionId(),
                'name' => $player->getName(),
                'status' => $player->getStatus()
            ]]]);
        } else if ($msg['action'] == ActionInterface::GET_OPPS) {
            $data = [];
            foreach ($this->clients->getPlayers() as $opponent) {
                if ($opponent !== $player) {
                    $data[] = [
                        'playerId' => $opponent->getConnectionId(),
                        'name' => $opponent->getName(),
                        'status' => $opponent->getStatus()
                    ];
                }
            }
            if ($data) {
                $this->clients->send($player->getConnectionId(), ['area' => 'opponents', 'action' => 'update', 'data' => $data]);
            }
        }
        if ($continue) {
            $isReady = true;
            foreach ($this->clients->getPlayers() as $profile) {
                if (!$profile->getDecision($turnId) && $profile->isOnGoing()) {
                    $isReady = false;
                }
            }
            if ($isReady) {
                $this->board->updatePlayers();
                $leftPlayers = $this->board->getLeftPlayers();
                if (count($leftPlayers) == count($this->clients->getPlayers())) {
                    $this->endRound($from);
                } else if ($card = $this->deck->draw()) {
                    try {
                        $this->board->playCard($card);
                        $this->round->nextTurn();
                    } catch (Trap $e) {
                        $this->clients->sendAll(['area' => 'board', 'action' => 'update', 'data' => [
                            'message' => "The Ship has sunk.",
                            'type' => 'red'
                        ]], Board::PLAY_CARD_DELAY);
                        $this->endRound($from);
                    }
                } else {
                    $this->endRound($from);
                }
            } else {
                $this->clients->send($player->getConnectionId(), ['area' => 'board', 'action' => 'update', 'data' => [
                    'message' => 'Waiting for other players...'
                ]]);
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        $this->clients->sendOthers($conn->resourceId, ['area' => 'opponents', 'action' => 'update', 'data' => [[
            'playerId' => $conn->resourceId,
            'disconnected' => true
        ]]]);
        $this->clients->getClients()->detach($conn);
        $this->clients->removePlayer($conn);
        if (!$this->clients->getPlayers()) {$this->board->clearAll();}

        echo "Connection {$conn->resourceId} has disconnected";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}";
        $this->clients->removePlayer($conn);
        $conn->close();
        if (!$this->clients->getPlayers()) {$this->board->clearAll();}
    }
    
    private function endRound(?ConnectionInterface $conn = null)
    {
        $this->board->clearRound();
        if (!$this->round->nextRound()) {
            $this->board->clearAll();
            $this->showScore($conn);
            $this->clients->removeAllPlayers();
        }
    }

    private function disconnect(ConnectionInterface $conn) {
        $conn->close();
    }
    
    private function showScore(ConnectionInterface $conn)
    {
        $data = [];
        $previousScore = $coins = $gems = $total = 0;
        foreach ($this->clients->getPlayers() as $player) {
            $chest = $player->getChest()->getTotal();
            $coins += $chest['coins'];
            $gems += $chest['gems'];
            $total += $chest['total'];
            $item = [
                'id' => $player->getConnectionId(),
                'name' => $player->getName(),
                'chest' => $chest,
                'self' => $player->getConnectionId() == $conn->resourceId
            ];
            $chest['total'] > $previousScore ? array_unshift($data, $item) : $data[] = $item;
            $previousScore = $chest['total'];
        }
        $data[] = [
            'name' => 'Total',
            'chest' => [
                'coins' => $coins,
                'gems' => $gems,
                'total' => $total
            ]
        ];
        $this->clients->sendAll(['area' => 'score', 'action' => 'update', 'data' => $data], 2.75);
    }
}