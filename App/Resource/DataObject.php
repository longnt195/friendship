<?php

namespace FriendShip\App\Resource;

class DataObject
{
    protected array $_data = [];
    
    public function __construct(
        array $data = []
    ) {
        $this->_data = $data;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function setData($key, $value = null): self
    {
        if ($key === (array)$key) {
            $this->_data = $key;
        } else {
            $this->_data[$key] = $value;
        }
        return $this;
    }

    /**
     * @param string $key
     * @return array|mixed|null
     */
    public function getData(string $key = ''): mixed
    {
        if ('' === $key) {
            return $this->_data;
        }
        
        return $this->_data[$key] ?? null;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->_data;
    }
}