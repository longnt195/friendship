<?php
namespace FriendShip\App\Api;

interface ActionInterface
{
    const GO = 'go';
    const LEAVE = 'leave';
    const JOIN = 'join';
    const GET_OPPS = 'getOpponents';
}