<?php
namespace FriendShip\App\Api;

interface CardInterface
{
    /**
     * @return string
     */
    public function getCardId(): string;

    /**
     * @param string $cardId
     * @return self
     */
    public function setCardId(string $cardId): self;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): self;

    /**
     * @return int
     */
    public function getValue(): int;

    /**
     * @param int $value
     * @return self
     */
    public function setValue(int $value): self;

    /**
     * @return string
     */
    public function getImgUrl(): string;
}