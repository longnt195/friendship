<?php
namespace FriendShip\App\Play;

use FriendShip\App\Client;

class Round
{
    const ROUND_MAX = 5;
    protected int $roundNo = 1;
    protected int $turn = 1;
    private Client $client;

    /**
     * @param Client $client
     */
    public function __construct(
        Client $client
    ) {
        $this->client = $client;
    }

    /**
     * @return bool
     */
    public function nextRound(): bool
    {
        if ($this->roundNo >= self::ROUND_MAX) {
            return false;
        }
        $this->roundNo++;
        $this->turn = 1;
        $this->client->sendAll(['area' => 'round', 'action' => 'update', 'data' => ['round' => $this->roundNo]], 2);
        return true;
    }
    
    public function nextTurn(): bool
    {
        $this->turn++;
        $this->client->sendAll(['area' => 'round', 'action' => 'update', 'data' => ['turn' => $this->getTurnId()]], Board::PLAY_CARD_DELAY);
        return true;
    }
    
    public function getTurnId(): string
    {
        return $this->roundNo . '.' . $this->turn;
    }
    
    public function reset(): void
    {
        $this->roundNo = $this->turn = 1;
    }
}