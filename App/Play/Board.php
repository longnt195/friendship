<?php
namespace FriendShip\App\Play;

use FriendShip\App\Api\ActionInterface;
use FriendShip\App\Cards\Card;
use FriendShip\App\Cards\Deck;
use FriendShip\App\Exception\Trap;
use FriendShip\App\Resource\DataObject;
use FriendShip\App\Player\Profile;
use FriendShip\App\Client;

class Board extends DataObject
{
    const MAX_PLAYERS = 8;
    const PLAY_CARD_DELAY = 1;
    /**
     * @var array 
     */
    private array $trapIDs = [];
    
    /**
     * @var Card[]
     */
    private array $gems = [];
    
    /**
     * @var Card[]
     */
    private array $coins = [];    
    
    /**
     * @var Card[]
     */
    private array $discardPile = [];
    
    private Round $round;
    private Deck $deck;
    private Client $client;

    /**
     * @param Deck $deck
     * @param Round $round
     * @param Client $client
     * @param array $data
     */
    public function __construct(
        Deck $deck,
        Round $round,
        Client $client,
        array $data = []
    ) {
        parent::__construct($data);
        $this->round = $round;
        $this->deck = $deck;
        $this->client = $client;
    }

    /**
     * @throws Trap
     */
    public function playCard(Card $card): void
    {
        $this->addCoins($card);
        $this->addGems($card);
        $trap = $this->addTrap($card);
        $this->client->sendAll(['area' => 'board', 'action' => 'add', 'data' => $card->getData()], self::PLAY_CARD_DELAY);
        if (!$trap) {
            throw new Trap();
        }
    }

    public function addCoins(Card $card): void
    {
        if ($card->getType() == Card::TYPE_COIN) {
            $this->coins[] = $card;
            /** @var Profile $player */
            $onGoingPlayers = array_filter($this->client->getPlayers(), function ($player) {
                return $player->isOnGoing();
            });
            $this->splitCoinCards([$card], $onGoingPlayers);
            foreach ($onGoingPlayers as $player) {
                $this->client->send($player->getConnectionId(), ['area' => 'profile', 'action' => 'update', 'data' => [
                    'tmpCoin' => $player->getTmpCoin()
                ]], self::PLAY_CARD_DELAY);
            }
        }
    }
    
    private function splitCoinCards(array $cards, array &$players): void
    {
        /** @var Card $card */
        foreach ($cards as $card) {
            $tmpCoin = floor($card->getValue() / count($players));
            /** @var Profile $player */
            foreach ($players as $player) {
                $player->addTmpCoin($tmpCoin);
            }
            $remainder = $card->getValue() - ($tmpCoin * count($players));
            $card->setValue((int)$remainder);
        }
    }

    public function addGems(Card $card): void
    {
        if ($card->getType() == Card::TYPE_GEM) {
            $this->gems[] = $card;
        }
    }

    public function addTrap(Card $card): bool
    {
        if ($card->getType() == Card::TYPE_TRAP) {
            $cardId = $card->getCardId();
            if (in_array($cardId, $this->trapIDs)) {
                $this->toDiscardPile($card);
                $this->clearRound();
                return false;
            }
            $this->trapIDs[] = $card->getCardId();
        }
        return true;
    }
    
    public function toDiscardPile(Card $card): self
    {
        $this->discardPile[] = $card;
        return $this;
    }
    
    public function clearRound(): void
    {
        $trapCount = 0;
        $this->clearAll(false);
        foreach ($this->discardPile as $heldCard) {
            $this->deck->discard($heldCard->getEntityId());
            if ($heldCard->getType() == Card::TYPE_TRAP) {
                $trapCard = $heldCard;
                $trapCount++;
            }
        }
        foreach ($this->client->getPlayers() as $player) {
            $player->reset();
        }
        $this->client->sendAll(['area' => 'board', 'action' => 'update', 'data' => [
            'message' => "Boarding a new Ship...",
            'clear' => true
        ]], 2);
        $this->client->sendAll(['area' => 'deck', 'action' => 'update', 'data' => ['count' => $this->deck->countCards()]], 2);
        if (isset($trapCard)) {
            $this->client->sendAll(['area' => 'discard', 'action' => 'update', 'data' => [
                'img_url' => $trapCard->getImgUrl(),
                'count' => $trapCount
            ]], 2);
        }
    }
    
    public function clearAll($reset = true): void
    {
        $this->deck->initCards();
        $this->coins = $this->gems = $this->trapIDs = $this->discardPile = [];
        if ($reset) {
            $this->round->reset();
        }
    }
    
    public function updatePlayers(): void
    {
        $turnId = $this->round->getTurnId();
        $leftPlayers = $this->getLeftPlayers($turnId);
        if ($leftPlayers) {
            $toUpdateCards = $this->coins;
            $this->splitCoinCards($this->coins, $leftPlayers);
            foreach ($leftPlayers as $player) {
                $player->storeCoins();
                if (count($leftPlayers) == 1) {
                    $player->storeGems($this->gems);
                    foreach ($this->gems as $gem) {
                        $this->toDiscardPile($gem);
                    }
                    $toUpdateCards = array_merge_recursive($this->coins, $this->gems);
                    $this->gems = [];
                }
                $player->setIsOnGoing(false);
                $player->setStatus('Left');
                $this->client->send($player->getConnectionId(), ['area' => 'profile', 'action' => 'update', 'data' => [
                    'tmpCoin' => $player->getTmpCoin(),
                    'chest' => $player->getChest()->getTotal(),
                    'status' => $player->getStatus(),
                    'lock' => true
                ]], self::PLAY_CARD_DELAY);
                $this->client->sendOthers($player->getConnectionId(), ['area' => 'opponents', 'action' => 'update', 'data' => [[
                    'playerId' => $player->getConnectionId(),
                    'status' => $player->getStatus(),
                    'onGoing' => $player->isOnGoing()
                ]]]);
            }
            $cardsData = [];
            foreach ($toUpdateCards as $card) {
                $cardsData[] = [
                    'entity_id' => $card->getEntityId(),
                    'value' => $card->getType() == Card::TYPE_GEM ? 'Taken' : $card->getValue()
                ];
            }
            $this->client->sendAll(['area' => 'board', 'action' => 'update', 'data' => [
                'cards' => $cardsData
            ]]);
        }

        $onGoingPlayers = array_filter($this->client->getPlayers(), function ($player) {
            return $player->isOnGoing();
        });
        foreach ($onGoingPlayers as $player) {
            $player->setStatus('Moo');
            $this->client->sendOthers($player->getConnectionId(), ['area' => 'opponents', 'action' => 'update', 'data' => [[
                'playerId' => $player->getConnectionId(),
                'status' => $player->getStatus(),
                'onGoing' => $player->isOnGoing()
            ]]]);
        }
    }

    /**
     * @param string $turnId
     * @return Profile[]
     */
    public function getLeftPlayers(string $turnId = ''): array
    {
        $players = $this->client->getPlayers();
        return array_filter($players, function ($player) use ($turnId) {
            return $turnId ? $player->getDecision($turnId) == ActionInterface::LEAVE :
                !$player->isOnGoing();
        });
    }
}