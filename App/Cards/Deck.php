<?php
namespace FriendShip\App\Cards;

use FriendShip\App\Client;

class Deck
{
    /**
     * @var Card[] 
     */
    private array $cards = [];
    private Client $client;

    /**
     * @param Client $client
     */
    public function __construct(
        Client $client
    ) {
        $this->initCards();
        $this->client = $client;
    }
    
    public function initCards(): void
    {
        $this->cards = [];
        foreach (DeckData::CARD_LIST as $key => $card) {
            $this->cards[$key] = new Card($card);
        }
    }
    
    public function draw(): ?Card
    {
        if ($this->cards) {
            $key = array_rand($this->cards);
            $card = $this->cards[$key];
            $this->discard($key);
            $this->client->sendAll(['area' => 'board', 'action' => 'update', 'data' => [
                'message' => 'Drawing a card...'
            ]]);
            $this->client->sendAll(['area' => 'deck', 'action' => 'update', 'data' => ['count' => $this->countCards()]]);
            return $card;
        } else {
            return null;
        }
    }
    
    public function discard($key): bool
    {
        unset($this->cards[$key]);
        return true;
    }
    
    public function countCards(): int
    {
        return count($this->cards);
    }
}