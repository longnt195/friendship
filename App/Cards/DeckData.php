<?php
namespace FriendShip\App\Cards;

class DeckData
{
    const CARD_LIST = [
        //coin cards
        1 => [
            Card::KEY_ENTITY_ID => 1,
            Card::KEY_CARD_ID => 'C1',
            Card::KEY_NAME => 'Golden-Eyes Idol',
            Card::KEY_TYPE => Card::TYPE_COIN,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/78642798.jpg',
            Card::KEY_ORIG_VALUE => 1,
            Card::KEY_VALUE => 1,
        ],
        2 => [
            Card::KEY_ENTITY_ID => 2,
            Card::KEY_CARD_ID => 'C2',
            Card::KEY_NAME => 'Silvervine Senri',
            Card::KEY_TYPE => Card::TYPE_COIN,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/13382806.jpg',
            Card::KEY_ORIG_VALUE => 2,
            Card::KEY_VALUE => 2,
        ],
        3 => [
            Card::KEY_ENTITY_ID => 3,
            Card::KEY_CARD_ID => 'C3',
            Card::KEY_NAME => 'Sonic Duck',
            Card::KEY_TYPE => Card::TYPE_COIN,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/84696266.jpg',
            Card::KEY_ORIG_VALUE => 3,
            Card::KEY_VALUE => 3,
        ],
        4 => [
            Card::KEY_ENTITY_ID => 4,
            Card::KEY_CARD_ID => 'C4',
            Card::KEY_NAME => 'Gem-Turtle',
            Card::KEY_TYPE => Card::TYPE_COIN,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/64734090.jpg',
            Card::KEY_ORIG_VALUE => 4,
            Card::KEY_VALUE => 4,
        ],
        5 => [
            Card::KEY_ENTITY_ID => 5,
            Card::KEY_CARD_ID => 'C5',
            Card::KEY_NAME => 'Ill Witch',
            Card::KEY_TYPE => Card::TYPE_COIN,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/81686058.jpg',
            Card::KEY_ORIG_VALUE => 5,
            Card::KEY_VALUE => 5,
        ],
        6 => [
            Card::KEY_ENTITY_ID => 6,
            Card::KEY_CARD_ID => 'C5',
            Card::KEY_NAME => 'Ill Witch',
            Card::KEY_TYPE => Card::TYPE_COIN,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/81686058.jpg',
            Card::KEY_ORIG_VALUE => 5,
            Card::KEY_VALUE => 5,
        ],
        7 => [
            Card::KEY_ENTITY_ID => 7,
            Card::KEY_CARD_ID => 'C7',
            Card::KEY_NAME => 'Dark Magician',
            Card::KEY_TYPE => Card::TYPE_COIN,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/46986414.jpg',
            Card::KEY_ORIG_VALUE => 7,
            Card::KEY_VALUE => 7,
        ],
        8 => [
            Card::KEY_ENTITY_ID => 8,
            Card::KEY_CARD_ID => 'C7',
            Card::KEY_NAME => 'Dark Magician',
            Card::KEY_TYPE => Card::TYPE_COIN,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/46986414.jpg',
            Card::KEY_ORIG_VALUE => 7,
            Card::KEY_VALUE => 7,
        ],
        9 => [
            Card::KEY_ENTITY_ID => 9,
            Card::KEY_CARD_ID => 'C9',
            Card::KEY_NAME => 'Gem-Knight Master Diamond',
            Card::KEY_TYPE => Card::TYPE_COIN,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/39512984.jpg',
            Card::KEY_ORIG_VALUE => 9,
            Card::KEY_VALUE => 9,
        ],
        10 => [
            Card::KEY_ENTITY_ID => 10,
            Card::KEY_CARD_ID => 'C11',
            Card::KEY_NAME => 'Dogmatika Nexus',
            Card::KEY_TYPE => Card::TYPE_COIN,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/22073844.jpg',
            Card::KEY_ORIG_VALUE => 11,
            Card::KEY_VALUE => 11,
        ],
        11 => [
            Card::KEY_ENTITY_ID => 11,
            Card::KEY_CARD_ID => 'C11',
            Card::KEY_NAME => 'Dogmatika Nexus',
            Card::KEY_TYPE => Card::TYPE_COIN,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/22073844.jpg',
            Card::KEY_ORIG_VALUE => 11,
            Card::KEY_VALUE => 11,
        ],
        12 => [
            Card::KEY_ENTITY_ID => 12,
            Card::KEY_CARD_ID => 'C12',
            Card::KEY_NAME => 'Diamond Crab King',
            Card::KEY_TYPE => Card::TYPE_COIN,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/7194917.jpg',
            Card::KEY_ORIG_VALUE => 12,
            Card::KEY_VALUE => 12,
        ],
        13 => [
            Card::KEY_ENTITY_ID => 13,
            Card::KEY_CARD_ID => 'C13',
            Card::KEY_NAME => 'Diamond Crab King',
            Card::KEY_TYPE => Card::TYPE_COIN,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/7194917.jpg',
            Card::KEY_ORIG_VALUE => 13,
            Card::KEY_VALUE => 13,
        ],
        14 => [
            Card::KEY_ENTITY_ID => 14,
            Card::KEY_CARD_ID => 'C14',
            Card::KEY_NAME => 'Diamond Crab King',
            Card::KEY_TYPE => Card::TYPE_COIN,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/7194917.jpg',
            Card::KEY_ORIG_VALUE => 14,
            Card::KEY_VALUE => 14,
        ],
        15 => [
            Card::KEY_ENTITY_ID => 15,
            Card::KEY_CARD_ID => 'C17',
            Card::KEY_NAME => 'Geonator Transverser',
            Card::KEY_TYPE => Card::TYPE_COIN,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/52119435.jpg',
            Card::KEY_ORIG_VALUE => 17,
            Card::KEY_VALUE => 17,
        ],
        //trap cards
        16 => [
            Card::KEY_ENTITY_ID => 16,
            Card::KEY_CARD_ID => 'T1',
            Card::KEY_NAME => 'Adhesion Trap Hole',
            Card::KEY_TYPE => Card::TYPE_TRAP,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/62325062.jpg',
            Card::KEY_ORIG_VALUE => 0,
            Card::KEY_VALUE => 0
        ],
        17 => [
            Card::KEY_ENTITY_ID => 17,
            Card::KEY_CARD_ID => 'T1',
            Card::KEY_NAME => 'Adhesion Trap Hole',
            Card::KEY_TYPE => Card::TYPE_TRAP,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/62325062.jpg',
            Card::KEY_ORIG_VALUE => 0,
            Card::KEY_VALUE => 0
        ],
        18 => [
            Card::KEY_ENTITY_ID => 18,
            Card::KEY_CARD_ID => 'T1',
            Card::KEY_NAME => 'Adhesion Trap Hole',
            Card::KEY_TYPE => Card::TYPE_TRAP,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/62325062.jpg',
            Card::KEY_ORIG_VALUE => 0,
            Card::KEY_VALUE => 0
        ],
        19 => [
            Card::KEY_ENTITY_ID => 19,
            Card::KEY_CARD_ID => 'T2',
            Card::KEY_NAME => 'Banishing Trap Hole',
            Card::KEY_TYPE => Card::TYPE_TRAP,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/89569453.jpg',
            Card::KEY_ORIG_VALUE => 0,
            Card::KEY_VALUE => 0
        ],
        20 => [
            Card::KEY_ENTITY_ID => 20,
            Card::KEY_CARD_ID => 'T2',
            Card::KEY_NAME => 'Banishing Trap Hole',
            Card::KEY_TYPE => Card::TYPE_TRAP,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/89569453.jpg',
            Card::KEY_ORIG_VALUE => 0,
            Card::KEY_VALUE => 0
        ],
        21 => [
            Card::KEY_ENTITY_ID => 21,
            Card::KEY_CARD_ID => 'T2',
            Card::KEY_NAME => 'Banishing Trap Hole',
            Card::KEY_TYPE => Card::TYPE_TRAP,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/89569453.jpg',
            Card::KEY_ORIG_VALUE => 0,
            Card::KEY_VALUE => 0
        ],
        22 => [
            Card::KEY_ENTITY_ID => 22,
            Card::KEY_CARD_ID => 'T3',
            Card::KEY_NAME => 'Card of Sacrifice',
            Card::KEY_TYPE => Card::TYPE_TRAP,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/88513608.jpg',
            Card::KEY_ORIG_VALUE => 0,
            Card::KEY_VALUE => 0
        ],
        23 => [
            Card::KEY_ENTITY_ID => 23,
            Card::KEY_CARD_ID => 'T3',
            Card::KEY_NAME => 'Card of Sacrifice',
            Card::KEY_TYPE => Card::TYPE_TRAP,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/88513608.jpg',
            Card::KEY_ORIG_VALUE => 0,
            Card::KEY_VALUE => 0
        ],
        24 => [
            Card::KEY_ENTITY_ID => 24,
            Card::KEY_CARD_ID => 'T3',
            Card::KEY_NAME => 'Card of Sacrifice',
            Card::KEY_TYPE => Card::TYPE_TRAP,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/88513608.jpg',
            Card::KEY_ORIG_VALUE => 0,
            Card::KEY_VALUE => 0
        ],        
        25 => [
            Card::KEY_ENTITY_ID => 25,
            Card::KEY_CARD_ID => 'T4',
            Card::KEY_NAME => 'Acid Trap Hole',
            Card::KEY_TYPE => Card::TYPE_TRAP,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/41356845.jpg',
            Card::KEY_ORIG_VALUE => 0,
            Card::KEY_VALUE => 0
        ],
        26 => [
            Card::KEY_ENTITY_ID => 26,
            Card::KEY_CARD_ID => 'T4',
            Card::KEY_NAME => 'Acid Trap Hole',
            Card::KEY_TYPE => Card::TYPE_TRAP,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/41356845.jpg',
            Card::KEY_ORIG_VALUE => 0,
            Card::KEY_VALUE => 0
        ],
        27 => [
            Card::KEY_ENTITY_ID => 27,
            Card::KEY_CARD_ID => 'T4',
            Card::KEY_NAME => 'Acid Trap Hole',
            Card::KEY_TYPE => Card::TYPE_TRAP,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/41356845.jpg',
            Card::KEY_ORIG_VALUE => 0,
            Card::KEY_VALUE => 0
        ],        
        28 => [
            Card::KEY_ENTITY_ID => 28,
            Card::KEY_CARD_ID => 'T5',
            Card::KEY_NAME => 'Eradicator Epidemic Virus',
            Card::KEY_TYPE => Card::TYPE_TRAP,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/54974237.jpg',
            Card::KEY_ORIG_VALUE => 0,
            Card::KEY_VALUE => 0
        ],
        29 => [
            Card::KEY_ENTITY_ID => 29,
            Card::KEY_CARD_ID => 'T5',
            Card::KEY_NAME => 'Eradicator Epidemic Virus',
            Card::KEY_TYPE => Card::TYPE_TRAP,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/54974237.jpg',
            Card::KEY_ORIG_VALUE => 0,
            Card::KEY_VALUE => 0
        ],
        30 => [
            Card::KEY_ENTITY_ID => 30,
            Card::KEY_CARD_ID => 'T5',
            Card::KEY_NAME => 'Eradicator Epidemic Virus',
            Card::KEY_TYPE => Card::TYPE_TRAP,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/54974237.jpg',
            Card::KEY_ORIG_VALUE => 0,
            Card::KEY_VALUE => 0
        ],
        //gem cards
        31 => [
            Card::KEY_ENTITY_ID => 31,
            Card::KEY_CARD_ID => 'G1',
            Card::KEY_NAME => 'Particle Fusion',
            Card::KEY_TYPE => Card::TYPE_GEM,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/39261576.jpg',
            Card::KEY_ORIG_VALUE => 5,
            Card::KEY_VALUE => 5
        ],
        32 => [
            Card::KEY_ENTITY_ID => 32,
            Card::KEY_CARD_ID => 'G2',
            Card::KEY_NAME => 'Particle Fusion',
            Card::KEY_TYPE => Card::TYPE_GEM,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/39261576.jpg',
            Card::KEY_ORIG_VALUE => 7,
            Card::KEY_VALUE => 7
        ],        
        33 => [
            Card::KEY_ENTITY_ID => 33,
            Card::KEY_CARD_ID => 'G3',
            Card::KEY_NAME => 'Particle Fusion',
            Card::KEY_TYPE => Card::TYPE_GEM,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/39261576.jpg',
            Card::KEY_ORIG_VALUE => 8,
            Card::KEY_VALUE => 8
        ],        
        34 => [
            Card::KEY_ENTITY_ID => 34,
            Card::KEY_CARD_ID => 'G4',
            Card::KEY_NAME => 'Gem-Knight Fusion',
            Card::KEY_TYPE => Card::TYPE_GEM,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/1264319.jpg',
            Card::KEY_ORIG_VALUE => 10,
            Card::KEY_VALUE => 10
        ],
        35 => [
            Card::KEY_ENTITY_ID => 35,
            Card::KEY_CARD_ID => 'G5',
            Card::KEY_NAME => 'Gem-Knight Fusion',
            Card::KEY_TYPE => Card::TYPE_GEM,
            Card::KEY_IMG_URL => 'https://images.ygoprodeck.com/images/cards/1264319.jpg',
            Card::KEY_ORIG_VALUE => 12,
            Card::KEY_VALUE => 12
        ]
    ];
    
    
}