<?php
namespace FriendShip\App\Cards;

use FriendShip\App\Resource\DataObject;
use FriendShip\App\Api\CardInterface;

class Card extends DataObject implements CardInterface
{
    const KEY_ENTITY_ID = 'entity_id';
    const KEY_CARD_ID = 'card_id';
    const KEY_NAME = 'name';
    const KEY_VALUE = 'value';
    const KEY_IMG_URL = 'img_url';
    const KEY_ORIG_VALUE = 'orig_value';
    const KEY_TYPE = 'type';
    const TYPE_COIN = 'coin';
    const TYPE_TRAP = 'trap';
    const TYPE_GEM = 'gem';

    public function __construct(
        array $data = []
    ) {
        parent::__construct($data);
    }

    /**
     * @return int
     */
    public function getEntityId(): int
    {
        return (int) $this->getData(self::KEY_ENTITY_ID);
    }

    /**
     * @inheritDoc
     */
    public function getCardId(): string
    {
        return (string) $this->getData(self::KEY_CARD_ID);
    }

    /**
     * @inheritDoc
     */
    public function setCardId(string $cardId): self
    {
        return $this->setData(self::KEY_CARD_ID, $cardId);
    }
    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return (string) $this->getData(self::KEY_NAME);
    }

    /**
     * @inheritDoc
     */
    public function setName(string $name): self
    {
        return $this->setData(self::KEY_NAME, $name);
    }

    /**
     * @inheritDoc
     */
    public function getType(): string
    {
        return (string) $this->getData(self::KEY_TYPE);
    }

    /**
     * @inheritDoc
     */
    public function getValue(): int
    {
        return (int) $this->getData(self::KEY_VALUE);
    }

    /**
     * @inheritDoc
     */
    public function setValue(int $value): self
    {
        return $this->setData(self::KEY_VALUE, $value);
    }

    /**
     * @inheritDoc
     */
    public function getImgUrl(): string
    {
        return (string) $this->getData(self::KEY_IMG_URL);
    }
}