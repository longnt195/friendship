<?php
namespace FriendShip\App;

use FriendShip\App\Player\Profile;
use Ratchet\ConnectionInterface;
use Ratchet\Server\IoServer;

class Client {

    private ?IoServer $server = null;
    protected \SplObjectStorage $clients;

    /**
     * @var Profile[]
     */
    protected array $players = [];

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
    }

    public function setServer(IoServer $server): self
    {
        $this->server = $server;
        return $this;
    }

    public function getClients(): \SplObjectStorage
    {
        return $this->clients;
    }

    /**
     * @return Profile[]
     */
    public function getPlayers(): array
    {
        return $this->players;
    }

    /**
     * @param string|int $id
     * @return Profile|null
     */
    public function getPlayerById(string|int $id): ?Profile
    {
        return $this->players[$id] ?? null;
    }

    public function addPlayer(ConnectionInterface $conn): Profile
    {
        if (!isset($this->players[$conn->resourceId])) {
            $this->players[$conn->resourceId] = new Profile($this);
            $this->players[$conn->resourceId]->setConnectionId($conn->resourceId);
        }
        return $this->players[$conn->resourceId];
    }
    
    public function removeAllPlayers(): void
    {
        $this->players = [];
    }

    public function removePlayer(ConnectionInterface $conn): void
    {
        unset($this->players[$conn->resourceId]);
    }

    public function sendAll($message, float $timer = 0): void
    {
        $this->server->loop->addTimer($timer, function () use ($message) {
            foreach ($this->getClients() as $conn) {
                $conn->send(is_string($message) ? $message : json_encode($message));
            }
        });
    }

    public function sendOthers($fromId, $message): void
    {
        foreach ($this->getClients() as $conn) {
            if ($conn->resourceId != $fromId) {
                $conn->send(is_string($message) ? $message : json_encode($message));
            }
        }
    }
    
    public function send($id, $message, float $timer = 0): void
    {
        $this->server->loop->addTimer($timer, function () use ($id, $message) {
            foreach ($this->getClients() as $conn) {
                if ($conn->resourceId == $id) {
                    $conn->send(is_string($message) ? $message : json_encode($message));
                }
            }
        });
    }
}