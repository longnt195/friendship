# FriendShip

##Install
1. With composer v2, pull vendor modules with the command
```
composer install
```

2. Docker setup can be found under ./server
```
cd server
docker-compose up
```
docker config here suggests the project to be installed under /var/www/html/FriendShip can be changed in docker-compose.yml

update server_name at ./server/config/nginx/sites/FriendShip.conf

##Launch
1. run the application
- with native php
```
php index.php $port $ip
```
e.g.
```
php index.php 8282
```
- with Docker installed
```
cd server
docker exec php php index.php 8282
```

2. open client on browsers
- add a param of port, which the app is listening to, to the url
```
?port=8282
```